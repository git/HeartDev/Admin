﻿namespace HeartTrack.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Contexte { get; set; }
        public string Description { get; set; }
        public Boolean isCheck { get; set; }
    }
}
