﻿using Blazorise;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace HeartTrack.Models
{
    public class TicketModel
    {
        [Required]
        [Range(0, 2500000)]
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The username must not exceed 50 characters.")]
        public string Username { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "The subject must not exceed 25 characters.")]
        public string Contexte { get; set; }

        [Required]
        [StringLength(500, ErrorMessage = "Description must not exceed 500 characters.")]
        public string Description { get; set; }
        public Boolean isCheck { get; set; } = false;
    }
}
