﻿using Blazorise;
using System.ComponentModel.DataAnnotations;

namespace HeartTrack.Models
{
    public class ReportModel
    {
        [Required]
        [Range(0, 2500000)]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [Required]
        [StringLength(50)]
        public string ReportedUser { get; set; }
        [Required]
        [StringLength(150)]
        public string Raison { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
        public Image Image { get; set; }
    }
}
