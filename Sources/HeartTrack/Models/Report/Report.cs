﻿using Blazorise;

namespace HeartTrack.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string ReportedUser { get; set; }
        public string Raison { get; set; }
        public string Description { get; set; }
        public Image Image { get; set; }
    }
}
