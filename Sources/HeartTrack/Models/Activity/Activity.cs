﻿namespace HeartTrack.Models
{
    public class Activity
    {
        public int IdActivity { get; set; }
        public string Type { get; set; }
        public DateOnly Date { get; set; }
        public DateOnly StartTime { get; set; }
        public DateOnly EndTime { get; set; }
        public int EffortRessenti { get; set; }
        public float Variability { get; set; }
        public float Variance { get; set; }
        public float StandardDeviation { get; set; }
        public float Average { get; set; }
        public int Maximum { get; set; }
        public int Minimum { get; set; }
        public float AvrTemperature { get; set; }
        public bool HasAutoPause { get; set; }
    }
}