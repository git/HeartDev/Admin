﻿namespace HeartTrack.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Sexe { get; set; }
        public float Taille { get; set; }
        public float Poids { get; set; }
        public DateTime BirthDate { get; set; }
        public Boolean isBan { get; set; }
    }
}
