﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HeartTrack.Models
{
    public class UserModel
    {
        [Required]
        [Range(0,2500000)]
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The username must not exceed 50 characters.")]
        public string Username { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The name must not exceed 50 characters.")]
        [RegularExpression(@"^[A-Za-z]$", ErrorMessage = "Numbers are not accepted.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "The last name must not exceed 25 characters.")]
        [RegularExpression(@"^[A-Za-z]*$", ErrorMessage = "Numbers are not accepted.")]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Sexe { get; set; }
        [Required]
        public float Taille { get; set; }
        [Required]
        public float Poids { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        public Boolean isBan { get; set; }
    }
}
