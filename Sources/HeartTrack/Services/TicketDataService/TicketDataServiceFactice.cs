﻿using System;
using Blazored.LocalStorage;
using HeartTrack.Models;
using HeartTrack.Services.TicketDataService;
using Microsoft.AspNetCore.Components;

namespace HeartTrack.Services.TicketDataServiceFactice
{
    public class TicketDataServiceFactice : ITicketDataService
    {
        [Inject]
        private HttpClient _clientHttp { get; set; }

        [Inject]
        public ILocalStorageService _localStorage { get; set; }

        [Inject]
        public NavigationManager _navigationManager { get; set; }

        private String EmplacementLocalStorage { get; set; }
        private String EmplacementJson { get; set; }


        public TicketDataServiceFactice(HttpClient clientHttp, ILocalStorageService localStorage, NavigationManager navigationManager)
        {
            this._clientHttp = clientHttp;
            this._localStorage = localStorage;
            this._navigationManager = navigationManager;
            this.EmplacementLocalStorage = "ticketsData";
            this.EmplacementJson = $"{_navigationManager.BaseUri}data/fake-tickets.json";
        }

        public async Task AddTicket(Ticket t)
        {
            List<Ticket> data = await getAllTickets();
            data.Add(t);
            await this.SaveAllTickets(data);
        }

        public async Task<Ticket> getTicketById(int id)
        {
            List<Ticket> tickets = await getAllTickets();
            Ticket? temp = null;

            foreach (Ticket t in tickets)
            {
                if (t.Id == id)
                {
                    temp = t;
                }
            }

            return temp;
        }

        public async Task<List<Ticket>> getAllTickets()
        {
            List<Ticket> lTickets = new List<Ticket>();

            lTickets = await this.getTicketsFromLocalStorage();
            if (lTickets.Count == 0)
            {
                lTickets = await this.getTicketsFromJson(this.EmplacementJson);
                await this.saveTicketsLocalStorage(lTickets);
            }

            return lTickets;
        }

        private async Task<List<Ticket>> getTicketsFromJson(String cheminVersJson)
        {
            List<Ticket> TicketsDeserialiser = new List<Ticket>();

            var data = await _clientHttp.GetFromJsonAsync<Ticket[]>(cheminVersJson);
            TicketsDeserialiser = data.ToList();

            return TicketsDeserialiser;
        }

        private async Task<List<Ticket>> getTicketsFromLocalStorage()
        {
            List<Ticket> TicketsFromLocalStorage = null;

            var data = await _localStorage.GetItemAsync<Ticket[]>(EmplacementLocalStorage);

            if (data == null)
            {
                TicketsFromLocalStorage = new List<Ticket>();
            }
            else
            {
                TicketsFromLocalStorage = data.ToList();
            }

            return TicketsFromLocalStorage;
        }

        public async Task RemoveTicket(Ticket t)
        {
            // Get the current data
            var currentData = await _localStorage.GetItemAsync<List<Ticket>>("data");

            // Get the item int the list
            var item = currentData.FirstOrDefault(w => w.Id == t.Id);

            // Delete item in
            currentData.Remove(item);

            // Save the data
            await _localStorage.SetItemAsync("data", currentData);
        }

        public async Task SaveAllTickets(List<Ticket> list)
        {
            await this.saveTicketsLocalStorage(list);
        }

        private async Task saveTicketsLocalStorage(List<Ticket> lTickets)
        {
            await _localStorage.SetItemAsync(this.EmplacementLocalStorage, lTickets);
        }

        public async Task UpdateTicket(Ticket t)
        {
            await this.RemoveTicket(t);
            await this.AddTicket(t);
        }

        public async Task Close(int Id)
        {
            // Get the current data
            var currentData = await _localStorage.GetItemAsync<List<Ticket>>("data");

            // Get the item int the list
            var item = currentData.FirstOrDefault(w => w.Id == Id);

            // Update item status
            item.isCheck = true;

            // Save the data
            await _localStorage.SetItemAsync("data", currentData);
        }
    }
}