﻿using System;
using HeartTrack.Models;
using Microsoft.AspNetCore.Components;

namespace HeartTrack.Services.TicketDataService
{
    public class TicketDataServiceAPI : ITicketDataService
    {
        [Inject]
        private HttpClient _clientHttp { get; set; }


        public TicketDataServiceAPI(HttpClient clientHttp)
        {
            this._clientHttp = clientHttp;
        }

        public async Task AddTicket(Ticket t)
        {
            HttpResponseMessage response = await _clientHttp.PostAsJsonAsync("http://localhost:8080/api/tickets", t);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - Ticket avec l'id " + t.Id + " ajouté avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème ajout Ticket");
            }
        }

        public async Task<Ticket> getTicketById(int id)
        {
            Ticket Ticket = await _clientHttp.GetFromJsonAsync<Ticket>("http://localhost:8080/api/tickets/{id}");
            return Ticket;
        }

        public async Task<List<Ticket>> getAllTickets()
        {
            List<Ticket> lTickets = await _clientHttp.GetFromJsonAsync<List<Ticket>>("http://localhost:8080/api/tickets");
            return lTickets;
        }

        public async Task RemoveTicket(Ticket t)
        {
            HttpResponseMessage response = await _clientHttp.DeleteAsync($"http://localhost:8080/api/tickets/{t.Id}");

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - Ticket avec l'id " + t.Id + " supprimé avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème suppression Ticket");
            }
        }

        public async Task SaveAllTickets(List<Ticket> list)
        {
            HttpResponseMessage response = await _clientHttp.PutAsJsonAsync("http://localhost:8080/api/tickets", list);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - List de tickets sauvegardé avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème sauvegarde List de tickets");
            }
        }

        public async Task UpdateTicket(Ticket t)
        {
            HttpResponseMessage response = await _clientHttp.PutAsJsonAsync($"http://localhost:8080/api/tickets/{t.Id}", t);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - Ticket avec l'id " + t.Id + " mis à jour avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème mise à jour Ticket");
            }
        }

        public async Task Close(int id)
        {

        }
    }
}