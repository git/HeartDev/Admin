﻿using System;
using HeartTrack.Models;

namespace HeartTrack.Services.TicketDataService
{
    public interface ITicketDataService
    {
        public Task<List<Ticket>> getAllTickets();

        public Task SaveAllTickets(List<Ticket> list);

        public Task AddTicket(Ticket t);

        public Task RemoveTicket(Ticket t);

        public Task UpdateTicket(Ticket t);

        public Task<Ticket> getTicketById(int id);
        public Task Close(int Id);
    }
}

