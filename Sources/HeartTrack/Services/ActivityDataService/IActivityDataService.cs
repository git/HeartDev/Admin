﻿using System;
using HeartTrack.Models;

namespace HeartTrack.Services.ActivityDataService
{
    public interface IActivityDataService
    {
        public Task<List<Activity>> getAllActivities();

        public Task SaveAllActivities(List<Activity> list);

        public Task AddActivity(Activity u);

        public Task RemoveActivity(Activity u);

        public Task UpdateActivity(Activity u);

        public Task<Activity> getActivityById(int id);
    }
}

