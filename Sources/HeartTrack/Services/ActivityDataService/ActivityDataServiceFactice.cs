﻿using System;
using Blazored.LocalStorage;
using HeartTrack.Models;
using HeartTrack.Services.ActivityDataService;
using Microsoft.AspNetCore.Components;

namespace HeartTrack.Services.ActivityDataServiceFactice
{
    public class ActivityDataServiceFactice : IActivityDataService
    {
        [Inject]
        private HttpClient _clientHttp { get; set; }

        [Inject]
        public ILocalStorageService _localStorage { get; set; }

        [Inject]
        public NavigationManager _navigationManager { get; set; }

        private String EmplacementLocalStorage { get; set; }
        private String EmplacementJson { get; set; }


        public ActivityDataServiceFactice(HttpClient clientHttp, ILocalStorageService localStorage, NavigationManager navigationManager)
        {
            this._clientHttp = clientHttp;
            this._localStorage = localStorage;
            this._navigationManager = navigationManager;

            this.EmplacementLocalStorage = "activitiesData";
            this.EmplacementJson = $"{_navigationManager.BaseUri}data/fake-activities.json";
        }

        public async Task AddActivity(Activity a)
        {
            List<Activity> data = await getAllActivities();
            data.Add(a);
            await this.SaveAllActivities(data);
        }

        public async Task<Activity> getActivityById(int id)
        {
            Console.WriteLine("Passage dans le getFromPseudo...");
            List<Activity> activities = await getAllActivities();
            Activity? temp = null;

            foreach (Activity a in activities)
            {
                if (a.IdActivity == id)
                {
                    temp = a;
                }
            }

            return temp;
        }

        public async Task<List<Activity>> getAllActivities()
        {
            List<Activity> lActivities = new List<Activity>();

            lActivities = await this.getActivitiesFromLocalStorage();
            if(lActivities.Count == 0)
            {
                lActivities = await this.getActivitiesFromJson(this.EmplacementJson);
                await this.saveActivitiesLocalStorage(lActivities);
            }

            return lActivities;
        }

        private async Task<List<Activity>> getActivitiesFromJson(String cheminVersJson)
        {
            List<Activity> activitiesDeserialiser = new List<Activity>();

            var data = await _clientHttp.GetFromJsonAsync<Activity[]>(cheminVersJson);
            activitiesDeserialiser = data.ToList();

            return activitiesDeserialiser;
        }

        private async Task<List<Activity>> getActivitiesFromLocalStorage()
        {
            List<Activity> activitiesFromLocalStorage = null;

            var data = await _localStorage.GetItemAsync<Activity[]>(EmplacementLocalStorage);

            if (data == null)
            {
                activitiesFromLocalStorage = new List<Activity>();
            }
            else
            {
                activitiesFromLocalStorage = data.ToList();
            }

            return activitiesFromLocalStorage;
        }

        public async Task RemoveActivity(Activity a)
        {
            List<Activity> data = await getAllActivities();

            int index = -1;

            foreach (Activity temp in data)
            {
                if (temp.IdActivity == a.IdActivity)
                {
                    index = data.IndexOf(temp);
                }
            }

            if (index != -1)
            {
                data.RemoveAt(index);
            }

            await this.SaveAllActivities(data);

            data = await this.getAllActivities();
        }

        public async Task SaveAllActivities(List<Activity> list)
        {
            await this.saveActivitiesLocalStorage(list);
        }

        private async Task saveActivitiesLocalStorage(List<Activity> lActivities)
        {
            await _localStorage.SetItemAsync(this.EmplacementLocalStorage, lActivities);
        }

        public async Task UpdateActivity(Activity a)
        {
            await this.RemoveActivity(a);
            await this.AddActivity(a);
        }
    }
}