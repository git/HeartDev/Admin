﻿using System;
using HeartTrack.Models;
using Microsoft.AspNetCore.Components;

namespace HeartTrack.Services.ActivityDataService
{
	public class ActivityDataServiceAPI : IActivityDataService
	{
		[Inject]
		private HttpClient _clientHttp { get; set; }


		public ActivityDataServiceAPI(HttpClient clientHttp)
		{
			this._clientHttp = clientHttp;
		}

		public async Task AddActivity(Activity a)
		{
			HttpResponseMessage response = await _clientHttp.PostAsJsonAsync("http://localhost:8080/api/activities", a);

			if (response.IsSuccessStatusCode)
			{
				Console.WriteLine("API - Activité avec l'id " + a.IdActivity + " ajouté avec succès");
			}
			else
			{
                Console.WriteLine("API - Problème ajout Activité");
            }
		}

		public async Task<Activity> getActivityById(int id)
		{
			Activity activity = await _clientHttp.GetFromJsonAsync<Activity>("http://localhost:8080/api/activities/{id}");
			return activity;
		}

		public async Task<List<Activity>> getAllActivities()
		{
			List<Activity> lActivities = await _clientHttp.GetFromJsonAsync<List<Activity>>("http://localhost:8080/api/activities");
			return lActivities;
		}

		public async Task RemoveActivity(Activity a)
		{
			HttpResponseMessage response = await _clientHttp.DeleteAsync($"http://localhost:8080/api/activities/{a.IdActivity}");

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - Activité avec l'id " + a.IdActivity + " supprimé avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème suppression Activité");
            }
		}

		public async Task SaveAllActivities(List<Activity> list)
		{
			HttpResponseMessage response = await _clientHttp.PutAsJsonAsync("http://localhost:8080/api/activities", list);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - List d'activités sauvegardé avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème sauvegarde List d'activités");
            }
		}

		public async Task UpdateActivity(Activity a)
		{
			HttpResponseMessage response = await _clientHttp.PutAsJsonAsync($"http://localhost:8080/api/activities/{a.IdActivity}", a);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("API - Activité avec l'id " + a.IdActivity + " mis à jour avec succès");
            }
            else
            {
                Console.WriteLine("API - Problème mise à jour Activité");
            }
		}
	}
}

