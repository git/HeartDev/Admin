﻿using HeartTrack.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace HeartTrack.Tests.Controller
{
    public class CultureControllerTests
    {
        [Fact]
        public void IsCultureControllerInstanceOfCultureController()
        {
            var culture = new CultureController();

            Assert.IsType<CultureController>(culture);
        }

        [Fact]
        public void IsSetCultureRedirecting()
        {
            var culture = new CultureController();

            Assert.IsAssignableFrom<IActionResult>(culture.SetCulture("",""));
        }
    }
}
