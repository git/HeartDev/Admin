﻿using HeartTrack.Shared;
using Xunit;

namespace HeartTrack.Tests.Shared
{
    public class MainLayoutTests
    {
        [Fact]
        public void IsMainLayoutInstanceOfMainLayout()
        {
            var main = new MainLayout();

            Assert.IsType<MainLayout>(main);
        }

        [Fact]
        public void StateChangeOnToggleNavMenu()
        {
            var main = new MainLayout();
            Assert.True(main.getCollapseNavMenu());
            main.ToggleNavMenu();
            Assert.False(main.getCollapseNavMenu());
        }
    }
}
