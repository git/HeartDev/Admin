﻿using HeartTrack.Shared;
using Xunit;

namespace HeartTrack.Tests.Shared
{
    public class CultureSelectorTests
    {
        [Fact]
        public void IsCultureSelectorInstanceOfCultureSelector()
        {
            var culture = new CultureSelector();

            Assert.IsType<CultureSelector>(culture);
        }

        [Fact]
        public void HasCultureEnoughLanguages()
        {
            var culture = new CultureSelector();

            Assert.Equal(2, culture.getSizeCultures());
        }
    }
}
