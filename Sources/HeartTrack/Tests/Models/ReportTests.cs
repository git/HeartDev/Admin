﻿using Blazorise;
using HeartTrack.Models;
using Xunit;

namespace HeartTrack.Tests.Models
{
    public class ReportTests
    {
        [Fact]
        public void IsReportInstanceOfReport()
        {
            var report = new Report();

            Assert.IsType<Report>(report);
        }

        [Fact]
        public void HasReportAttributeRightInstace() 
        {
            var report = new Report();

            Assert.IsType<int>(report.Id);
            Assert.IsType<string>(report.Username);
            Assert.IsType<string>(report.ReportedUser);
            Assert.IsType<string>(report.Raison);
            Assert.IsType<string>(report.Description);
            Assert.IsType<Image>(report.Image);
        }

        [Fact]
        public void IsReportModelValidate()
        {
            var report = new ReportModel();

            Assert.True(report.Id < 2500000);
            Assert.True(report.Username.Length < 50 && report.Username.Length>0);
            Assert.True(report.ReportedUser.Length < 50 && report.ReportedUser.Length > 0);
            Assert.True(report.Raison.Length < 150 && report.Raison.Length > 0);
            Assert.True(report.Description.Length < 500 && report.Description.Length > 0);
        }
    }
}
