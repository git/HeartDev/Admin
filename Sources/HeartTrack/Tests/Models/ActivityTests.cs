﻿using HeartTrack.Models;
using Xunit;

namespace HeartTrack.Tests.Models
{
    public class ActivityTests
    {
        [Fact]
        public void IsActivityInstanceOfActivity()
        {
            var activity = new Activity();
            Assert.IsType<Activity>(activity);
        }

        [Fact]
        public void HasActivityAttrivutesRightInstance()
        {
            var activity = new Activity();

            Assert.IsType<int>(activity.IdActivity);
            Assert.IsType<string>(activity.Type);
            Assert.IsType<DateOnly>(activity.Date);
            Assert.IsType<DateOnly>(activity.StartTime);
            Assert.IsType<DateOnly>(activity.EndTime);
            Assert.IsType<int>(activity.EffortRessenti);
            Assert.IsType<float>(activity.Variability);
            Assert.IsType<float>(activity.Variance);
            Assert.IsType<float>(activity.StandardDeviation);
            Assert.IsType<float>(activity.Average);
            Assert.IsType<int>(activity.Maximum);
            Assert.IsType<int>(activity.Minimum);
            Assert.IsType<float>(activity.AvrTemperature);
            Assert.IsType<bool>(activity.HasAutoPause);

        }
    }
}
