﻿using Xunit;
using HeartTrack.Models;

namespace HeartTrack.Tests.Models
{
    public class UserTests
    {
        [Fact]
        public void IsUserInstanceOfUser()
        {
            var user = new User();

            Assert.IsType<User>(user);
        }

        [Fact]
        public void HasUserAttributeRightInstance() 
        {
            var user = new User();

            Assert.IsType<int>(user.Id);
            Assert.IsType<string>(user.Username);
            Assert.IsType<string>(user.Nom);
            Assert.IsType<string>(user.Prenom);
            Assert.IsType<string>(user.Email);
            Assert.IsType<string>(user.Password);
            Assert.IsType<string>(user.Sexe);
            Assert.IsType<float>(user.Taille);
            Assert.IsType<float>(user.Poids);
            Assert.IsType<DateTime>(user.BirthDate);
            Assert.IsType<Boolean>(user.isBan);
        }

        [Fact]
        public void IsUserModelValidate()
        {
            var user = new UserModel();

            Assert.NotNull(user.Username);
            Assert.NotNull(user.FirstName);
            Assert.NotNull(user.LastName);
            Assert.NotNull(user.Email);
            Assert.NotNull(user.Password);
            Assert.NotNull(user.Sexe);

            Assert.True(user.Id<2500000);
            Assert.True(user.Username.Length >= 0 && user.Username.Length < 50);
            Assert.True(user.FirstName.Length >= 0 && user.FirstName.Length < 50);
            Assert.True(user.LastName.Length >= 0 && user.LastName.Length < 25);
        }
    }
}
