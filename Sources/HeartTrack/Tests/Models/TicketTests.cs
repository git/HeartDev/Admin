﻿using HeartTrack.Models;
using Xunit;

namespace HeartTrack.Tests.Models
{
    public class TicketTests
    {
        [Fact]
        public void IsTicketInstanceOfTicket()
        {
            var ticket = new Ticket();

            Assert.IsType<Ticket>(ticket);
        }

        [Fact]
        public void HasTicketAttributeRightInstance() 
        { 
            var ticket = new Ticket();

            Assert.IsType<int>(ticket.Id);
            Assert.IsType<string>(ticket.Username);
            Assert.IsType<string>(ticket.Contexte);
            Assert.IsType<string>(ticket.Description);
            Assert.IsType<Boolean>(ticket.isCheck);
        }

        [Fact]
        public void IsTicketModelValidate()
        {
            var ticket = new TicketModel();

            Assert.True(ticket.Id < 2500000);
            Assert.True(ticket.Username.Length > 0 && ticket.Username.Length < 50);
            Assert.True(ticket.Contexte.Length > 0 && ticket.Contexte.Length < 25);
            Assert.True(ticket.Description.Length > 0 && ticket.Description.Length < 500);
            Assert.True(ticket.isCheck);
        }
    }
}
