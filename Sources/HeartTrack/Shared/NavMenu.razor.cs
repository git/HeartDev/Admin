﻿using HeartTrack.Pages;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace HeartTrack.Shared
{
    public partial class NavMenu
    {
        [Inject]
        public IStringLocalizer<NavMenu> Localizer { get; set; }
    }
}
