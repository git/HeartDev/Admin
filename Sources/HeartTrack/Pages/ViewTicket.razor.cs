﻿using HeartTrack.Models;
using HeartTrack.Services;
using HeartTrack.Services.TicketDataService;
using Microsoft.AspNetCore.Components;

namespace HeartTrack.Pages
{
    public partial class ViewTicket
    {
        [Parameter]
        public int Id { get; set; }

        private Ticket ticket { get; set; } = new();

        [Inject]
        public ITicketDataService TicketService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        protected async Task OnInitializedAsync()
        {
            var item = await TicketService.getTicketById(Id);

            ticket = new Ticket
            {
                Id = item.Id,
                Username = item.Username,
                Contexte = item.Contexte,
                Description = item.Description,
                isCheck = item.isCheck
            };
        }

        private void OnNavigateOnReturnClicked()
        {
            NavigationManager.NavigateTo("/tickets", true);
        }
    }
}