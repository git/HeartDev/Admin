﻿using Blazored.LocalStorage;
using Blazorise;
using Blazorise.DataGrid;
using HeartTrack.Models;
using HeartTrack.Services;
using HeartTrack.Services.TicketDataService;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using System;
using static MudBlazor.CategoryTypes;

namespace HeartTrack.Pages
{
    public partial class Tickets
    {
        private List<Ticket> tickets;

        private int totalTicket;

        private DataGrid<Ticket> dataGrid;

        [Inject]
        public HttpClient Http { get; set; }

        [Inject]
        public ILocalStorageService LocalStorage { get; set; }

        [Inject]
        public ITicketDataService TicketService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        private ISnackbar Snackbar { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            // Do not treat this action if is not the first render
            if (!firstRender)
            {
                return;
            }

            var currentData = await LocalStorage.GetItemAsync<Ticket[]>("data");

            // Check if data exist in the local storage
            if (currentData == null)
            {
                // this code add in the local storage the fake data (we load the data sync for initialize the data before load the OnReadData method)
                var originalData = Http.GetFromJsonAsync<Ticket[]>($"{NavigationManager.BaseUri}fake-tickets.json").Result;
                await LocalStorage.SetItemAsync("data", originalData);
            }

        }

        private async Task OnReadData(DataGridReadDataEventArgs<Ticket> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            // When you use a real API, we use this follow code
            //var response = await Http.GetJsonAsync<Data[]>( $"http://my-api/api/data?page={e.Page}&pageSize={e.PageSize}" );
            var response = (await LocalStorage.GetItemAsync<Ticket[]>("data")).Skip((e.Page - 1) * e.PageSize).Take(e.PageSize).ToList();

            if (!e.CancellationToken.IsCancellationRequested)
            {
                totalTicket = (await LocalStorage.GetItemAsync<List<Ticket>>("data")).Count;
                tickets = new List<Ticket>(response); // an actual data for the current page
            }
        }

        private async void OnClose(int id)
        {
            await TicketService.Close(id);

            NavigationManager.NavigateTo("tickets", true);
            Snackbar.Add("Ticket fermé !");
    }

        private void OnView(int id)
        {
            NavigationManager.NavigateTo("tickets/view/"+id);
        }

        private void OnNavigateOnAddClicked()
        {
            NavigationManager.NavigateTo("tickets/add");
        }

        private async void OnDelete(Ticket t)
        {
            await TicketService.RemoveTicket(t);

            NavigationManager.NavigateTo("tickets", true);
        }
    }
}
