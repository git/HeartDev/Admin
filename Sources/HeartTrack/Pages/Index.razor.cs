﻿using MudBlazor;

namespace HeartTrack.Pages
{
    public partial class Index
    {
        private int IndexChart = -1;

        public ChartOptions Options = new ChartOptions();

        public List<ChartSeries> Series = new List<ChartSeries>()
        {
            new ChartSeries() { Name = "Views", Data = new double[] { 90, 79, 72, 69, 62, 62, 55, 65, 70 } }
        };
        
        public string[] XAxisLabels = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep" };
    }
}
