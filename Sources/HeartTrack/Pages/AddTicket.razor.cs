﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components;
using HeartTrack.Models;
using HeartTrack.Services;
using HeartTrack.Services.TicketDataService;

namespace HeartTrack.Pages
{
    public partial class AddTicket
    {
        
        [Inject]
        public ITicketDataService TicketStorage { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        public TicketModel ticketModel = new(){};

        private async void HandleValidSubmit()
        {
            var currentData = await TicketStorage.getAllTickets();
            ticketModel.Id = currentData.Max(s => s.Id) + 1;

            currentData.Add(new Ticket
            {
                Id = ticketModel.Id,
                Username = ticketModel.Username,
                Contexte = ticketModel.Contexte,
                Description = ticketModel.Description/*,
                Urgence = ticketModel.Urgence*/
            });

            await TicketStorage.SaveAllTickets(currentData);
            NavigationManager.NavigateTo("tickets");
        }

    }
}
