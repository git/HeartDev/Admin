﻿using System;
using System.Net.Http;
using Blazorise.DataGrid;
using HeartTrack.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using HeartTrack.Services.ActivityDataService;

namespace HeartTrack.Pages
{
	public partial class Activities
	{
        private List<Activity> activities;

        private int totalActivity;

        [Inject]
        private IActivityDataService ActivitiesDataService { get; set; }
        [Inject]
        public IStringLocalizer<Activities> Localizer { get; set; }
        private async Task OnReadData()
        {
            this.activities = await this.ActivitiesDataService.getAllActivities();
            this.totalActivity = activities.Count();
        }
    }
}

