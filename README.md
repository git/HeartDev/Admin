<div align = center>

  <h1>HeartTrack - Admin</h1>
</div>


<div align = center>



---
&nbsp; ![Blazor](https://img.shields.io/badge/Blazor-56478C?style=for-the-badge&logo=Blazor&logoColor=FFFFFF&labelColor=56478C)
&nbsp; ![CSS](https://img.shields.io/badge/CSS-000?style=for-the-badge&logo=css3&logoColor=white&color=darkblue)
&nbsp; ![.NET](https://img.shields.io/badge/.NET-56478C?style=for-the-badge&logo=.NET&logoColor=FFFFFF&labelColor=56478C)
</br>

</div>  
  
# Table des matières
[Présentation](#présentation) | [Fonctionnalités principales](#fonctionnalités-principales) | [Répartition du Git](#répartition-du-git) | [Prerequis](#prérequis) | [Fabriqué avec](#fabriqué-avec) | [Contributeurs](#contributeurs) | [Comment contribuer](#comment-contribuer) | [License](#license) | [Remerciements](#remerciements)



## Présentation

**Nom de l'application :** HeartTrack

### Contexte

HeartTrack est une application web PHP et mobile Android destinée aux sportifs et aux coachs afin de permettre l'analyse de courbes de fréquences cardiaques et le suivi d'équipe sportive. L'objectif principal de cette application est de récupérer les données de fréquence cardiaque à partir de fichiers .FIT, de les afficher sous forme de courbes, d'identifier des paternes, de fournir des statistiques et de réaliser des prédictions liées à l'effort physique, à la chaleur, à la récupération, etc.

### Récapitulatif du Projet

Ce dépôt contient une application Blazor conçue pour faciliter l'administration des utilisateurs au sein de votre application. L'application utilise le framework Blazor, qui permet le développement d'applications web interactives avec .NET.

## Fonctionnalités principales

1. **Gestion des Utilisateurs**: Ajoutez, modifiez et supprimez des utilisateurs de manière simple et efficace.

2. **Rôles Utilisateurs**: Attribuez des rôles à chaque utilisateur pour définir leurs permissions et accès.

3. **Tableau de Bord Convivial**: Interface utilisateur intuitive pour une expérience d'administration agréable.

4. **Sécurité Intégrée**: Utilisation des fonctionnalités de sécurité de Blazor pour protéger les données sensibles.

## Les attendus du projet
* [x] Implementation of a data visualization page with pagination (2 points)
* [x] Page for adding an element with validation (2 point)
* [x] Edit page of an element without validation (2 point)
* [x] Deletion of an element with a confirmation (2 point)
* [ ] Complex component (5 point)
* [x] Use API (Get / Insert / Update / Delete) (3 point)
* [x] IOC & DI use (4 point)
* [x] Localization & Globalization (at least two languages) (1 point)
* [ ] Use of the configuration (1 point)
* [ ] Logs (2 points)
* [x] Code cleanliness (2 point)
* [x] GUI (Global design, placement of buttons, ...) (2 point)
* [x] Code location (No code in views) (2 point)
* [x] The Readme (2 points)
* [x] Description of how the client solution works (code-level illustration) (6 points)
* [x] Merge request (2 points)

## Ce que nous avons fait
Pour d'avantages d'informations, voir les branches `issue_auth` et `merged_APE`
* [x] Les listing dans la page de tickets et d'Activités
* [x] La modification dans la page de tickets
* [x] La suppression dans la page de tickets
* [x] Le get by id dans la page de tickets
* [x] L'implementation de la partie API et du data service dans la pages d'Activités
* [x] L'authentification dans la branch `issue_auth` mais des problèmes persistent (les actions des services ne sont pas gerer par l'authentification)
* [ ] Le data service dans toutes les pages
* [ ] La répartitions des fonctionnalités de la page de tickets sur les autres pages

## Répartition du Git

[**Sources**](Sources/HeartTrack) : **Code de l'application**

---

Le projet HeartTrack utilise un modèle de flux de travail Git (Gitflow) pour organiser le développement. Voici une brève explication des principales branches :

- **branche WORK-NAME** : Cette branche contient la version de travail des différents membres de l'équipe. Les modifications sur cette branche sont bien souvent réalisé par le nom du membre en question.

- **branche master** : La branche master est similaire à la branche de production, mais elle contient la version stable et final de notre application. 

### API PHP

L'application Blazor utilise une API en PHP pour récupérer les données depuis la base de données. 
Pour l'utiliser, vous devez faire tourner notre projet PHP disponible [ici](https://codefirst.iut.uca.fr/git/HeartDev/Web/src/branch/API_tests) sur votre machine locale. Pour ensuite changer le type de skockage `ActivityDataServiceFactice` par `ActivityDataServiceAPI` et vice versa dans le fichier `Program.cs` de l'application Blazor en ligne 24 `Add Data Services`.


### Prérequis
* [Visual Studio](https://visualstudio.microsoft.com/fr/vs/) - IDE
* [Git](https://git-scm.com/) - Gestion de version
* [.NET SDK](https://dotnet.microsoft.com/download) - Platform open-source pour le développement d'applications

## Installation et Exécution

1. Clonez ce dépôt sur votre machine locale :

    ```bash
    git clone https://codefirst.iut.uca.fr/git/HeartDev/Admin.git
    ```

2. Lacer Visual Studio et ouvrez le projet `HeartTrack.sln` dans le dossier `Sources/HeartTrack`.

Pour des raison de manque de la partie php, vous ne pourrez utiliser l'application qu'en localStorage. Il est cependant possible de tester l'aaplication avec la partie API en PHP en faisant tourner en local le projet php disponible [ici](https://codefirst.iut.uca.fr/git/HeartDev/Web/src/branch/API_tests) et en changant le type de skockage `ActivityDataServiceFactice` par `ActivityDataServiceAPI` dans le fichier `Program.cs` de l'application Blazor en ligne 24 `Add Data Services` et importer les dépendances en conséquence.   


Comme ceci : 
```csharp
// Add Data Services
builder.Services.AddScoped<IActivityDataService, ActivityDataServiceAPI>();
builder.Services.AddScoped<ITicketDataService, TicketDataServiceAPI>();
```

### Fabriqué avec
* [Visual Studio](https://visualstudio.microsoft.com/fr/vs/) - IDE
* [CodeFirst](https://codefirst.iut.uca.fr/) - Gitea
  * [Drone](https://codefirst.iut.uca.fr/) - CI (Intégration Continue)
  * [SonarQube](https://codefirst.iut.uca.fr/sonar/) - Qualité du Code
* [Blazor](https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor) - Framework Web
  * [C#](https://docs.microsoft.com/fr-fr/dotnet/csharp/) - Langage
  * [HTML 5, CSS 3](https://developer.mozilla.org/fr/docs/Web/HTML) - Langages

## Contributeurs
* [Antoine PEREDERII](https://codefirst.iut.uca.fr/git/antoine.perederii)
* [Paul LEVRAULT](https://codefirst.iut.uca.fr/git/paul.levrault)
* [Kevin MONTEIRO](https://codefirst.iut.uca.fr/git/kevin.monteiro)
* [Antoine PINAGOT](https://codefirst.iut.uca.fr/git/antoine.pinagot)
* [David D'HALMEIDA](https://codefirst.iut.uca.fr/git/david.d_almeida)

## Comment contribuer
1. Forkez le projet (<https://codefirst.iut.uca.fr/git/HeartDev/Admin>)
2. Créez votre branche (`git checkout -b feature/featureName`)
3. commit vos changements (`git commit -am 'Add some feature'`)
4. Push sur la branche (`git push origin feature/featureName`)
5. Créez une nouvelle Pull Request


## License
Ce projet est sous licence ``MIT`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations.

## Remerciements
Ce projet a été réalisé dans le cadre de la SAÉ Projet Web et Mobile de l'IUT de Clermont-Ferrand.